package com.example.webmobtaskapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import com.example.webmobtaskapplication.Api.ApiClient;
import com.example.webmobtaskapplication.Api.ApiInterface;
import com.example.webmobtaskapplication.Bean.Data;
import com.example.webmobtaskapplication.Bean.Example;
import com.example.webmobtaskapplication.Bean.PurchasedOfficeService;
import com.example.webmobtaskapplication.Bean.PurchasedOfficeTemplate;
import com.example.webmobtaskapplication.Bean.PurchasedService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvSubjectList;
    ApiInterface apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        init();
        getData();
    }


    void getData() {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.setCancelable(false);
        progress.show();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvSubjectList.setLayoutManager(layoutManager);
        if (isOnline()) {
            apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<Example> call = apiService.getpurchasedList();
            call.enqueue(new Callback<Example>() {
                @Override
                public void onResponse(Call<Example> call, Response<Example> response) {
                    if (response.body().getData() != null) {
                        Data data = response.body().getData();
                        List<PurchasedService> purchasedServices =data.getPurchasedServices();
                        if(purchasedServices.size() >0) {
                            PurchasedService purchasedService = purchasedServices.get(0);
                            PurchasedOfficeTemplate purchasedOfficeTemplate = purchasedService.getPurchasedOfficeTemplate();
                            List<PurchasedOfficeService> purchasedOfficeServices = purchasedOfficeTemplate.getPurchasedOfficeServices();
                            rvSubjectList.setAdapter(new Adapter(MainActivity.this,purchasedOfficeServices));
                        }


                    } else {
                        Toast.makeText(MainActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                    }
                    progress.dismiss();
                }

                @Override
                public void onFailure(Call<Example> call, Throwable t) {
                    Toast.makeText(MainActivity.this, "Server Not Responding", Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                }
            });
        } else {
            showNetworkAlert(true);
        }
    }
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


    void init() {
        rvSubjectList = (RecyclerView) findViewById(R.id.rvList);

    }
    public void showNetworkAlert(final boolean isFininsh) {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("Internet Connecation Required");
        builder.setMessage("please check your internet connection");

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                if (isFininsh)
                    finish();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (isFininsh)
                    finish();
            }
        });
        builder.show();
    }

}