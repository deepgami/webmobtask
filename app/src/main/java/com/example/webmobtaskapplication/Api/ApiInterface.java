package com.example.webmobtaskapplication.Api;

import com.example.webmobtaskapplication.Bean.Example;
import com.example.webmobtaskapplication.Bean.PurchasedOfficeService;

import retrofit2.Call;
import retrofit2.http.GET;



public interface ApiInterface {
    @GET("https://api.jsonbin.io/b/5efdf1000bab551d2b6ab1c9/1")
    Call<Example> getpurchasedList();
}
