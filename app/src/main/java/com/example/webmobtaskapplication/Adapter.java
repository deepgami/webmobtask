package com.example.webmobtaskapplication;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.webmobtaskapplication.Bean.PurchasedOfficeService;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.Myview> {
    Activity context;
    List<PurchasedOfficeService> dataList;
    RecyclerView rvSubjectList;


    public Adapter(Activity context, List<PurchasedOfficeService> dataList) {
        this.context=context;
        this.dataList = dataList;
    }

    @Override
    public Myview onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_main, viewGroup, false);

        return new Myview(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Myview myview, final int i) {
        myview.name.setText(dataList.get(i).getName());
        myview.price.setText(dataList.get(i).getPrice());
        Glide.with(context).load(dataList.get(i).getImage()).into(myview.ivIcon);



    }

    @Override
    public int getItemCount()
    {
        return dataList.size();
    }

    public class Myview extends RecyclerView.ViewHolder {
        TextView name,price;
        ImageView ivIcon;
        LinearLayout llsubjectlist;
        RecyclerView rvSubjectList;

        public Myview(@NonNull View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price);
            ivIcon=(ImageView)itemView.findViewById(R.id.ivIcon);
            rvSubjectList=itemView.findViewById(R.id.rvSubjectList);
             }
    }
}